#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass letter
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 0
\use_package amssymb 0
\use_package cancel 0
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 0
\use_package mhchem 1
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\topmargin 3cm
\rightmargin 3cm
\bottommargin 3cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout My Address
Guillaume Jeanmairet
\begin_inset Newline newline
\end_inset

Max Planck Institute FKF
\begin_inset Newline newline
\end_inset

Heisenbergstraße 1, 70569 Stuttgart,
\begin_inset Newline newline
\end_inset

 Germany
\end_layout

\begin_layout Send To Address
The Journal of Chemical Physics
\begin_inset Newline newline
\end_inset

 AIP Publishing LLC
\begin_inset Newline newline
\end_inset

1305 Walt Whitman Road
\begin_inset Newline newline
\end_inset

Suite 300
\begin_inset Newline newline
\end_inset

Melville, NY 11747-4300, USA
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
Red fields are mandatory, green ones optional.
\end_layout

\end_inset


\end_layout

\begin_layout Signature
Dr.
 Guillaume Jeanmairet
\begin_inset Newline newline
\end_inset


\begin_inset Graphics
	filename signature.pdf
	width 25text%

\end_inset


\end_layout

\begin_layout Opening
Dear Editor,
\end_layout

\begin_layout Standard
Please find here the responses to the referees concerning our article proposed
 for publication as 
\shape italic
article
\shape default
 in the Journal of Chemical Physics (Manuscript A16.10.0212):
\end_layout

\begin_layout Standard
\align center

\shape italic
Stochastic multi-reference perturbation theory with application to linearized
 coupled cluster method
\end_layout

\begin_layout Standard
\align center
by Sandeep Sharma, Ali Alavi and myself.
\end_layout

\begin_layout Standard

\bar under
First Reviewer:
\end_layout

\begin_layout Standard

\shape italic
-
\begin_inset Quotes erd
\end_inset

At two places in the manuscript, the ”neci” code is mentioned without any
 information.
 The acronym should be explicited and a reference should be given.
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
At the first occurence of the neci term, we explained that it is the name
 of our FCIQMC code.
 On page 4 we replaced : 
\begin_inset Quotes eld
\end_inset

In the third part a description of the algorithmic of the QMC-LCC method
 that has been implemented in the neci code is given, we also discuss some
 important technical points.
 
\begin_inset Quotes erd
\end_inset

 by 
\begin_inset Quotes eld
\end_inset

In the third part a description of the algorithmic of the QMC-LCC method
 that has been implemented in our FCIQMC code, neci, is given, we also discuss
 some important technical points.
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
We also replaced the 
\begin_inset Quotes eld
\end_inset

NECI
\begin_inset Quotes erd
\end_inset

 by 
\begin_inset Quotes eld
\end_inset

neci
\begin_inset Quotes erd
\end_inset

 on page 8.
 There are no technical paper about this code yet, so we do not have relevant
 reference to cite.
\end_layout

\begin_layout Standard

\shape italic
-
\begin_inset Quotes erd
\end_inset

In caption of Table I it is written that E2 energies predicted by MPS-LCC
 and LCC-QMC are given.
 However, it is not the case.
 It should be corrected.
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
We know just specify that those energies correspond to LCC-QMC, in the caption
 of Tab.I.
\end_layout

\begin_layout Standard

\shape italic
-
\begin_inset Quotes erd
\end_inset

In the two tables the error bars on LCC-QMC data should be given.
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard

\bar under
second reviewer:
\end_layout

\begin_layout Standard
-
\begin_inset Quotes erd
\end_inset


\shape italic
in the sentence: 
\shape default
indeed in fciqmc we do not have access to a proper description of the zeroth
 order wavefunction.
 [p.
 7]
\shape italic
 the authors are probably referring to the fact that fciqmc is stochastic
 and thus the wavefunction is affected by what we can describe as a stochastic
 noise, differently from the deterministic approaches.
 still, both approaches are within an accuracy given by convergence thresholds,
 thus i would not use the words “proper description” since for a large enough
 number of walkers the errors in the determinantal expansion could (always)
 be limited.

\shape default
 
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
we fully agree with the referee that in the limit of a infinitely precise
 calculation the exact wavefunction is always recovered.
 what we wanted to say was that we do not have access to a good estimation
 of the wavefunction because we could not store all the visited determinants
 because of memory limitation.
 thus we replaced the improper expression mentioned by the referee by :
 
\begin_inset Quotes eld
\end_inset

indeed in fciqmc the dynamics takes place in the ci space and it is not
 possible to store all the populated determinants, thus we do not have access
 to zeroth order wavefuntion.
\begin_inset Quotes erd
\end_inset

 on page 7.
\end_layout

\begin_layout Standard

\shape italic
-in page 10 the authors introduce a new method to keep under control the
 walkers’ population’s growth in both the reference space Ψ0 and in that
 of the first order correction Ψ1.
 For what I’ve understood they compare this method to the initiator approximatio
n first introduced in ref.
 43.
 If this is the case, it would be reasonable to briefly recall the initiator
 approximation in the computational details.
\end_layout

\begin_layout Standard
\begin_inset Quotes eld
\end_inset

We agree with the reviewer.
 We described the initiator approximation the first time we mentioned it
 (p8).
 By adding the following:
\end_layout

\begin_layout Standard
The initiator method i-FCIQMC was developed to reduce the number of walkers
 required to sample efficiently the wavefunction, and it does so extremely
 effectively, by making a very small change to the algorithm.
 An additional criterion is stipulated for walkers spawned onto new (i.e.
 empty) determinants.
 Those newly spawned walkers are allowed to survive only if their parent
 resides on a determinant designated as an initiator.
 Determinants are considered initiators when their walker population exceeds
 a critical number.
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard

\shape italic
-
\begin_inset Quotes erd
\end_inset

In Table I the authors should write in the caption that the results are
 related to the Benzene molecule.
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
We added the mention to the benzene molecule if caption of Table 1.
\end_layout

\begin_layout Standard
-
\begin_inset Quotes erd
\end_inset


\shape italic
Figure 3 could be made clearer: above each point of the blue curve the alpha
 value could be inserted, and for each point of the black curve the threshold
 value.
 If possible the curves could be made dotted.
 Moreover the black and blue colors are too similar, better increase the
 contrast.
 I also suggest to put the number of walkers in 10
\begin_inset Formula $^{7}$
\end_inset

 format.
\shape default

\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
We modified the figure 2 according to reviewer's recommendations.
 
\end_layout

\begin_layout Standard

\shape italic
-
\begin_inset Quotes erd
\end_inset

Regarding the sentence: Moreover in the case of the (-2, 0, 2) and (-1,
 -1, 2) classes it was not possible to do the calculation.
 [p.
 17] the authors should try to give a more (if possible) extended explanation,
 on the nature of these convergence issues.
 For example, can you plot the convergence as a function of the steps.
 How does the energy oscillate? Is there a relation between the population
 and the energy fluctuations, or on the contrary despite the increase of
 the number of walkers the energy doesn’t converge? Moreover, since the
 same excitations converge well in the benzene molecule, could the difficulties
 depend on the diradical character of m-xylylene? Could this be the case
 for the (-1, -1, 2) class of excitations? Can the authors discuss this
 point further?
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
We clarified why those calculation cannot be done by adding on page 17:
 
\begin_inset Quotes eld
\end_inset

the calculations become unfeasible because the number of walkers on |Ψ1>
 keep growing until the memory limitation of the computer is reached.
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
We also mention that in our current understanding the reasons for this explosion
 of the number of walkers in case of the m-xylylene is due to the size of
 the pertruber spaces:
\end_layout

\begin_layout Standard
\begin_inset Quotes eld
\end_inset

We believe that those classes are harder, i.e.
 require much more walkers to reach a mEH accuracy because the size of the
 corresponding perturber spaces increases when moving from the benzene to
 the m-xylylene.
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
-
\begin_inset Quotes erd
\end_inset


\shape italic
In the conclusions: 
\shape default
However it has also been demonstrated that the proposed algorithm is still
 not adapted to study such complicated systems since the class of excitation
 involving two cores electrons and to virtuals holes cannot be computed
 for the m-xylylene molecule.
 This problem has been circumvent by computing those classes by using the
 contracted MPS- LCC technique.
 This point out the necessity to develop a similar contracted approach with
 our fully stochastic procedure, this is currently under investigation.
 [p.
 18]
\shape italic
 the authors seem to suggest that the convergence issue is related to the
 fact that in LCCQMC, the stochastic sampling develops in an uncontracted
 MPS-LCC space, differently from its deterministic equivalent.
 Surely the contraction would simplify the convergence but could the authors
 describe the gain/loss of this kind of approximation, and are they certain
 that it could overcome the convergence issues
\shape default
?
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
First we want to correct the referee on a point: the LCCQMC is still done
 in the CI space, not in the MPS-LCC space.
\end_layout

\begin_layout Standard
I addition we precise why the contraction would speed up the calculation
 and which impact it would have on the results by adding the sentence:
\end_layout

\begin_layout Standard
\begin_inset Quotes eld
\end_inset

In such approximation the perturbation is no longer expressed and sampled
 in the determinants space but in the much smaller space generated by applying
 all possible excitation on the frozen |Ψ0⟩, which should make the calculation
 more manageable.
 This implies that the electrons in the active space are no longer allowed
 to relax, this approximation will be responsible of an underestimation
 of the response energy but it seems that this error is moderate in the
 LCC framework.
\begin_inset Quotes erd
\end_inset

 in the conclusion.
\end_layout

\begin_layout Standard

\shape italic
-
\begin_inset Quotes erd
\end_inset

In this regard an explicit comparison of the computational costs and of
 the E2 energies, obtained with both the MPS-LCC and the LCCQMC procedures
 for the Benzene molecule, could help to better underline the power of the
 stochastic approach.
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
-
\shape italic

\begin_inset Quotes erd
\end_inset

Finally the following sentences require some revisions:
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Standard
We corrected the different sentences mentioned by the reviewer.
\end_layout

\begin_layout Closing
On behalf of the authors,
\end_layout

\end_body
\end_document
